import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {BasketComponent} from './basket.component';
import {ProductDetailComponent} from './product.detail.component';

@Component({
    selector: 'my-app',
    template: `<h1>Simple product buying demo with components iteractions</h1>
            <div style='float: right; border: 3px solid'>
                <basket></basket>
            </div>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [product]="product" (productBuyEvent) = "onProductBuyEvent($event)"></product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [BasketComponent, ProductDetailComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];
     @ViewChild(BasketComponent) basketComponent:BasketComponent;

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }

    onProductBuyEvent(product: ProductType) : void {
        this.basketComponent.buy(product);
    }
}