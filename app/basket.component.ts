import {Component} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'basket',
    template: `
        <div>
            <img src='http://histmag.org/static/1393709076_shopping-cart.png' />
            $To pay: {{toPayAmount}}</div>
            <button (click)='emptyBasket()' style='float: right'>CLEAR BASKET</button>
    `
})
export class BasketComponent {
    private toPayAmount: number = 0;

    public buy(product: ProductType): void {
        this.toPayAmount = this.toPayAmount + product.price;
    }

    public emptyBasket(): void {
        this.toPayAmount = 0;
    }
}