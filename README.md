# Writing applications in Angular 2, [part 5] #

Okay I already showed you many ways of how Angular 2 components can interact with each other. Let's take a look at another really popular way 
of how component can interact with the outside world and that's called "**Parent listens for child event**" with using of popular **EventEmitter**
**which pushes events up to parent** of a component from where EventEmmiter was defined and invoked. 

## Parent listens for child event (EventEmitter) ##

### Principle ###

So far every named component interaction way didn't push us to define usual interface of a component in terms of what's INPUT and what's
component OUTPUT. Let's try that with Angular 2 decorators **Input** and **Output**. Where Input defines what's component input and 
Output is used for** event bound output property of a component**. In the other words, with property marked by the Output decorator you 
can say **what's going to be pushed up to any parent component** which is going to be using our component. Let's show that on example. 

First, let's modify our example with simple product list and basket and create new product detail component.

```
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'product-detail',
    template: `
        <div>{{product.name}} for {{product.price}}$ <button (click)="fireProductBuyEvent(product)">BUY</button></div>
        <img src={{product.image}} />
    `    
})

export class ProductDetailComponent {
    @Input() product: ProductType;
    @Output() productBuyEvent = new EventEmitter<ProductType>();

    fireProductBuyEvent(product: ProductType) : void {
        this.productBuyEvent.emit(product);
    }
}
```
* Via the Input decorator we marked **input property where we'll push the input from parent component**.
* Via the Output decorator we defined EventEmitter **which will push up to parent product that our user selected for insertion into basket**.

Okay, now let's **listen for that event in the component which is using our product-detail**...

```
import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {BasketComponent} from './basket.component';
import {ProductDetailComponent} from './product.detail.component';

@Component({
    selector: 'my-app',
    template: `<h1>Simple product buying demo with components iteractions</h1>
            <div style='float: right; border: 3px solid'>
                <basket></basket>
            </div>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [product]="product" (productBuyEvent) = "onProductBuyEvent($event)"></product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [BasketComponent, ProductDetailComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];
     @ViewChild(BasketComponent) basketComponent:BasketComponent;

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }

    onProductBuyEvent(product: ProductType) : void {
        this.basketComponent.buy(product);
    }
}
```

Now important part, by the code "**(productBuyEvent) = "onProductBuyEvent($event)"**" I'm saying: "**Hey, everytime productBuyEvent is emited in the child component call parent's AppComponent .onProductBuyEvent method and pass event argument in there**". Angular 2 will always inject emited payload through the $event variable. You can't choose something else!

And that's it!..:) Choosing of a product should again update the basket as before but now with help of EventEmitter.

## Testing the demo ##

* git clone <this repo>
* npm install
* npm start
* visit http://localhost:3000

Hope you found this usefull

regards

Tomas